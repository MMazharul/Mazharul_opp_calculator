<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 5/24/2017
 * Time: 2:05 PM
 */

namespace App;


class Cal
{
protected $number1;
protected $number2;
protected $operation;
    public function setdata($value)
    {
        if(array_key_exists('number1',$value))
        {
            $this->number1=$value['number1'];
        }
        if(array_key_exists('number2',$value))
        {
            $this->number2=$value['number2'];
        }
        if(array_key_exists('operation',$value))
        {
            $this->operation=$value['operation'];
        }
    }
    public function calculate()
    {
        switch($this->operation)
        {
            case 'addition':  return ($this->number1+$this->number2);
            case 'subtraction':  return ($this->number1-$this->number2);
            case 'multiplication':  return ($this->number1*$this->number2);
            case 'division':  return ($this->number1/$this->number2);
            default: return "Error";

        }

    }
}